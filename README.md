# Suivi des projets du DREAL datalab

**Objectif :** produire les supports de présentation du suivi des projets du DREAL datalab dans le cadre des réunions annuelles avec les services

## Paramétrage du diaporama

Ouvrir le fichier diaporama-automatique.Rmd.

### Lien vers la feuille de calcul

Affecter l'URL vers la Google sheet à la variable :
`google_sheet="url_to_google_sheet_edit"`

### Date et service concernés

Indiquer la date du diaporama et le service concerné aux lignes 17-18 :
```
params:
  date: "2024-10-16"
  service: "MECC"
```

La **date** paramétrée à cet endroit détermine l'époque du bilan (objet `epoque_bilan` calculé ligne 68). 
Cet objet peut valoir : 'début d'année', 'mi-année' ou 'fin d'année', en fonction du mois de `params$date`. 
Vérifier que le programme répond conformément aux attentes.

Pour créer les diapositives de tous les projets de  **tous les services**, mettre le paramètre `service` à `"*"`.
Cela rendra inopérant le filtre de la liste des projets par service.

## Authentification Google

Dans la page navigateur qui s'ouvre, donner l'accès à Tidyverse API Packages au compte Google que vous souhaitez utiliser.
