---
title: "État du portefeuille de projets à fin 2024"
author: "Ronan Vignard"
date: '`r Sys.Date()`'
output:
  xaringan::moon_reader:
    lib_dir: libs
    css: xaringan-themer.css
    self_contained: true
    seal: false
    nature:
      highlightStyle: github
      highlightLines: true
      ratio: 16:9
      countIncrementalSlides: false
params:
  date: "2024-10-21"
  service: "*"
---

```{r setup, eval=TRUE, echo=FALSE, message=FALSE, warning=FALSE}
# remotes::install_github("gadenbuie/epoxy")
library(epoxy)
library(googlesheets4)
library(tidyverse)
library(lubridate)
library(xaringanthemer)
library(xaringanExtra)
# remotes::install_github("hadley/emo")
library(emo)

gs4_deauth()
nb_car_tronquage = 500
google_sheet <- "https://docs.google.com/spreadsheets/d/1wDZaFWy5PaMqrIGvqa40S9w6p-KvWaWxCHHzPtJZdgs"

```


```{r xaringan-themer, include=FALSE, warning=FALSE}
style_duo_accent(
  primary_color = "#000091",
  secondary_color = "#E18B63",
  inverse_header_color = "#FFFFFF",
  text_font_family = "Marianne"
)
```

```{r xaringan-extra, include=FALSE, warning=FALSE}
xaringanExtra::use_tachyons()
```

```{r xaringan-extra-styles, include=FALSE, warning=FALSE}
xaringanExtra::use_extra_styles(
  hover_code_line = TRUE,
  mute_unhighlighted_code = TRUE
)
```


```{r init_base_projets, eval=TRUE, echo=FALSE, message=FALSE, warning=FALSE}
projets <- read_sheet(google_sheet, sheet = "Portefeuille") %>% 
  mutate(`Statut du projet` = factor(`Statut du projet`, 
                                     levels = c("6. Actif", 
                                                "9. MCO avec demande(s) limitée(s) d’évolution fonctionnelle",
                                                "8. MCO avec mise à jour périodique des données", 
                                                "7. MCO", 
                                                "3. Non démarré SCTE", 
                                                "1. Proposé", 
                                                "2. Non démarré client", 
                                                "4. En stand-by client", 
                                                "5. En stand-by SCTE",  
                                                "10. Livré sans maintenance", 
                                                "11. Abandonné")),
         across(c(`Résultats à atteindre 6 mois`, `Objectifs généraux`, `Avancement`, `Bilan semestre/année écoulé.e`), 
                 ~ if_else(nchar(.x) > 500, paste0(substr(.x, 1, 500), "..."), .x))
         ) %>% 
  filter(grepl(params$service, `Service Client`), 
         `Statut du projet` != "11. Abandonné", 
         `Statut du projet` != "10. Livré sans maintenance") %>% 
  rename(`Equipe projet` = `Equipe projet (chef de projet en gras)`) %>% 
  arrange(`Service Client`, `Division cliente`, `Référent client`, `Priorité du service`, `Statut du projet`)

epoque_bilan <- case_when(
  month(params$date) %in% 10:12 ~ paste("fin", year(params$date)),
  month(params$date) %in% 1:2 ~ paste("fin", year(params$date)-1),
  month(params$date) %in% 3:5 ~ paste("début", year(params$date)),
  TRUE ~ paste("mi", year(params$date))
  )

```
class: center, middle, title-slide


.yellow.f1[DREAL datalab] 

.white.f1[Projets `r params$service` à `r epoque_bilan`] 

---

```{epoxy diapo_proj, eval=FALSE}
<h2>{`Nom du projet`} ({`Priorité du service`} {`Service Client`} - {`Année de référence`}) </h2>
  
.pull-left[
<h3>Objectifs du projet</h3>

{`Objectifs généraux`}

.yellow.b[Type de projet]  : {`Type de projet`}

.yellow.b[Statut du projet] : {`Statut du projet`}

.yellow.b[Référent client]  : {`Référent client`}
]

.pull-right[

<h3>Bilan {epoque_bilan} </h3>

.yellow.b[Avancement] : {`Avancement`}

{`Bilan semestre/année écoulé.e`}

.yellow.b[Coût estimé] : {`Coût estimé SCTE`}

.yellow.b[Équipe projet]  : {`Equipe projet`}
]

.yellow.b[Références] : {`Liens (fiche-projet, répertoire de travail, produit fini)`}

---
```

```{epoxy ref.label="diapo_proj", data = projets}
```

```{r pdf, eval=FALSE, echo=FALSE}
services <- projets %>% 
  mutate(`Service Client` = if_else(`Service Client` == "Direction", "SCTE", `Service Client`)) %>% 
  pull(`Service Client`) %>% 
  unique() %>% sort() %>% 
  setdiff(c("MECC / SRNT", "SIAL", "MECC")) 

creer_ppt_service <- function(svce = "SRNT") {
    rmarkdown::render(input = "slides/diaporama-automatique.Rmd", 
                      output_file = paste0(Sys.Date(), "_diapo_auto_drealdatalab_", svce, ".html"),
                      output_dir = "slides/slides_svc/", params =  list(date = Sys.Date(), service = svce))
}

lapply(X = services, FUN = creer_ppt_service)

# purrr::map(list.files(path = "slides/slides_svc/", pattern = ".html", full.names = TRUE), ~pagedown::chrome_print(.x))
# pagedown::chrome_print("slides/diaporama-automatique.html")

```

class: middle, center, title-slide

# Merci de votre attention `r emo::ji("pray")`

<br>
<br>

.bg-washed-blue.b--dark-blue.ba.bw2.br3.ph4.mt5[
[Consulter le portefeuille de projets](https://docs.google.com/spreadsheets/d/1wDZaFWy5PaMqrIGvqa40S9w6p-KvWaWxCHHzPtJZdgs/edit?usp=sharing)
]

