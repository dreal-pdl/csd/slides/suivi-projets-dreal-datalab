---
title: "Plan d'action 2021 du Dreal datalab"
author: ""
date: '`r Sys.Date()`'
output:
  xaringan::moon_reader:
    lib_dir: libs
    css: xaringan-themer.css
    self_contained: true
    seal: false
    nature:
      highlightStyle: github
      highlightLines: true
      ratio: 16:9
      countIncrementalSlides: false
---

```{r xaringan-themer, include=FALSE, warning=FALSE}
library(xaringanthemer)
style_duo_accent(
  primary_color = "#000091",
  secondary_color = "#E18B63",
  inverse_header_color = "#FFFFFF",
  text_font_family = "Marianne"
)
```

```{r xaringan-extra, include=FALSE, warning=FALSE}
library(xaringanExtra)
xaringanExtra::use_tachyons()
```

```{r xaringan-extra-styles, include=FALSE, warning=FALSE}
xaringanExtra::use_extra_styles(
  hover_code_line = TRUE,
  mute_unhighlighted_code = TRUE
)
```


class: center, middle, title-slide


.yellow.f1[DREAL datalab]

.white.f1[Plan d'action 2021] 

---
class: inverse, center, middle
# Contexte

---
## Contexte 2021

- Besoin de consolidation / développement des compétences
- Besoin de consolidation / refactorisation des produits réalisés
- Contexte sanitaire : 
  - disponibilité des connexions
  - accès au patrimoine de données via le VPN
  - difficulté de mettre en place des séances de créativité
  
  => dégradation de la productivité
- Projet de service 
- Projet "Service public de la connaissance"
- Mobilité de Catherine Chamard-Bois, Christian Rincé et mobilité de Maël Theulière
- Beaucoup d'attentes des clients

---
class: inverse, center, middle
# Synthèse
---
## Synthèse : Répartition des projets par service et statut

```{r, echo=FALSE, message=FALSE, warning=FALSE}
library(formattable)

df <- tibble::tribble(
  ~`Service client`, ~Actif, ~`Maintenance évolutive`, ~MCO, ~`En stand-by`, ~`Abandonné`, ~`Ensemble des projets`,
           "MECC",     3L,                     0L,   0L,             7L,         1L,                   11L,
           "SCTE",     2L,                     1L,   1L,             1L,         0L,                    5L,
           "SIAL",     2L,                     1L,   2L,             3L,         2L,                   10L,
           "SRNP",     5L,                     2L,   0L,             9L,         0L,                   16L,
           "SRNT",     2L,                     2L,   0L,             1L,         0L,                    5L,
    "SRNT / MECC",     0L,                     0L,   1L,             0L,         0L,                    1L,
           "STRV",     0L,                     0L,   0L,             5L,         0L,                    5L,
          "Dreal",    14L,                     6L,   4L,            26L,         3L,                   53L
  )

unit.scale = function(x) x / 53

formattable(df, list(Actif = color_bar(color = "#E18B63", fun = unit.scale),
                     `Maintenance évolutive` = color_bar(color = "#E18B63", fun = unit.scale),
                     MCO = color_bar(color = "#E18B63", fun = unit.scale),
                     `En stand-by` = color_bar(color = "#E18B63", fun = unit.scale),
                     `Abandonné` = color_bar(color = "#E18B63", fun = unit.scale),
                     `Ensemble des projets` = color_bar(color = "#E18B63", fun = unit.scale)))
```
---
class: center, middle

# Projet en cours ou qui auront de premiers livrables d'ici 2021
---

class: inverse, center, middle
# MECC
---

## Portail démarche simplifié pour les méthaniseurs

.pull-left[
### Objectifs du projet

Demande d’un appui SCTE sur un outil de type démarche simplifiée et aide à l’exploitation des données, sur le modèle de ce qui a été déjà mis en place en Bretagne (1ere réunion envisagée mi février 2020)


.yellow.b[Type de projet]  : Projet en cours

.yellow.b[statut du projet] : Actif

.yellow.b[Référent client]  : Nathalie BOURGEAIS
]

.pull-right[
### Objectif septembre 2021

Publication SIGloire installations en fonctionnement 2019 (campagne 2020), transposition des traistements de redressement de AILE en instructions R pour péréniser la méthodologie


.yellow.b[Coût estimé] : important (plusieurs semaines)

.yellow.b[Pilote du projet]  : Juliette Engelaere-Lefebvre
]

---

## Enquête réseaux de chaleur

.pull-left[
### Objectifs du projet
  
Actualiser les données sur les réseaux de chaleur en augmentant la qualité des réponses et en tenant compte des données déjà disponibles par ailleurs

.yellow.b[Type de projet]  : Projet en cours

.yellow.b[Statut de projet]  : Actif

.yellow.b[Référent client]  : Albin Péronnie

]

.pull-right[

### Objectif septembre 2021

Mise à jour de la couche SIGloire.

.yellow.b[Coût estimé] : faible (qq jours)

.yellow.b[Pilote du projet]  : Juliette Engelaere-Lefebvre

]

---
## propre.energie

.pull-left[
### Objectifs du projet
  
Réalisation d’une publication / application automatisée et mutualisée en interdreal sur l’énergie (consommations art 179 publieées par le SDES)
  
.yellow.b[Type de projet]  : Projet nouveau

.yellow.b[Statut de projet]  : Actif

.yellow.b[Référent client]  : A définir

]

.pull-right[

### Objectif septembre 2021

Définition de la maquette de l'application, datapréparation données 2008-2019, formations réalisées

  
.yellow.b[Coût estimé] : important (plusieurs semaines)

.yellow.b[Pilote du projet]  : Juliette Engelaere-Lefebvre

]

---
class: inverse, center, middle

# SCTE
---

## propre.consommation d’espace

.pull-left[
### Objectifs du projet
  
Réalisation d’une publication automatisée sur la consommation d’espace.

.yellow.b[Type de projet]  : Projet nouveau

.yellow.b[Statut de projet]  : Actif

.yellow.b[Référent client]  : Gwénaëlle Le Bourhis

]

.pull-right[

### Objectif septembre 2021

Réalisation d’une publication automatisée sur la consommation d’espace.

    
.yellow.b[coût estimé] : modéré à important

.yellow.b[Pilote du projet]  : Maël Theulière

]
---

## Indicateurs territoriaux

.pull-left[
### Objectifs du projet
  
Automatiser la collecte et l’administration des indicateurs territoriaux, faciliter leur mise à disposition DREAL, DDTM et collectivités.

.yellow.b[Type de projet]  : Projet en cours

.yellow.b[Statut de projet]  : Actif

.yellow.b[Référent client]  : David Goutx ?

]

.pull-right[

### Objectif septembre 2021

Finalisation du processus d'alimentation des données, mise en ligne d'une version diffusable de l'application

.yellow.b[coût estimé] : important (plusieurs semaines)

.yellow.b[Pilote du projet]  : Maël Theulière

]
---
## Gestion du Code officiel géographique
.pull-left[

### Objectifs du projet
  
Automatiser la gestion de la carte des territoires : 
 - gestion des fusion des communes
 - calculs automatiques pour les territoires supra communaux
 - intégration des fonds de carte correspondant

.yellow.b[Type de projet]  : Projet en maintenance

.yellow.b[Statut de projet]  : Maintenance évolutive

.yellow.b[Référent client]  : Pas nécessaire

]

.pull-right[

### Objectif septembre 2021

- Mise à jour des données 2021
- Passage de la récupération des données via API
- Amélioration des fonctions existantes 
- Ajout d’autres zonages Insee
- Stockage des données en base

.yellow.b[coût estimé] : faible (qq jours)

.yellow.b[Pilote du projet]  : Maël Theulière

]
---
## Catalogue de donnée

.pull-left[

### Objectifs du projet
  
Structurer les pratiques des agents producteurs et utilisateurs de données en interne DREAL

.yellow.b[Type de projet]  : Projet en stand by

.yellow.b[Statut de projet]  : Maintenance évolutive

.yellow.b[Référent client]  : Pas nécessaire

]

.pull-right[

### Objectif septembre 2021

Déploiement d’une instance DKAN2 fonctionnelle en interne DREAL pour effectuer un test grandeur nature.

.yellow.b[coût estimé] : modéré (qq semaines)

.yellow.b[Pilote du projet]  : Ronan Vignard
]
---
class: inverse, center, middle

## SIAL
---
## propre.rpls


.pull-left[
### Objectifs du projet
  
Cas d’école de la méthode propre sur les données du parc social.

.yellow.b[Type de projet]  : Projet en maintenance

.yellow.b[Statut de projet]  : Maintenance évolutive

.yellow.b[Référent client]  : Julien Caudrelier

]

.pull-right[

### Objectif septembre 2021

- Mise à jour 2021
- Formation des nouveaux membres de l'équipe
- Intégration des évolutions qui n'avaient pas put être pris en charge en 2020
- Refactorisation

.yellow.b[coût estimé] : modéré (qq semaines)

.yellow.b[Pilote du projet]  : Maël Theulière

]

.yellow.b[Remarque] : Projet mutualisé en inter-DREAL

---

## Étude sur la connaissance des marchés fonciers et immobiliers

.pull-left[
### Objectifs du projet
  
Améliorer la connaissance des dynamiques des marchés immobiliers et fonciers, par :

- la construction de quelques indicateurs permettant de disposer de références de prix par territoire;
- leur prise en compte dans les analyses menées par SIAL/DPH  ;
- leur valorisation au travers de différents supports.

.yellow.b[Type de projet]  : Projet en cours

.yellow.b[Statut de projet]  : En stand-by

.yellow.b[Référent client]  : Julien Caudrelier, Elsa Bertin

]

.pull-right[

### Objectif septembre 2021

- Définition des indicateurs pertinents
- chaine de calcul automatique de ces indicateurs dans le cadre du projet IT
- production d'outil de valorisation / communication sur ces indicateurs

.yellow.b[coût estimé] : modéré (qq semaines)

.yellow.b[Pilote du projet]  : Gwénaëlle Le Bourhis

.yellow.b[Remarque]  : Pourra être relancé après propre.artificialisation
]
---

## POC zonages habitat

.pull-left[
### Objectifs du projet
  
Automatiser la mise à jour des zonages habitat, socle de base des données de l'habitat (PIG, OPAH, parc public...) et favoriser sa diffusion en interne DREAL (PAC habitat) et auprès des partenaires extérieurs.

.yellow.b[Type de projet]  : Projet en cours

.yellow.b[Statut de projet]  : Actif

.yellow.b[Référent client]  : Julien Caudrelier, Elsa Bertin

]

.pull-right[

### Objectif septembre 2021

Intégration des zonages prioritaires pour le client, mise en place de l'administration de données correspondantes, production d'un pack qgis zonages habitat, publications des zones sur SIGloire.

.yellow.b[coût estimé] : important (plusieurs semaines)

.yellow.b[Pilote du projet]  : Juliette Engelaere-Lefebvre

]
---

## POC Copropriétés V1

.pull-left[
### Objectifs du projet
  
- Repérer les co-propriétés de la région non inscrites dans le registre national.
- Créer des indicateurs de fragilité des co-propriétés.
- Diffuser la connaissance à un cercle restreint d’acteurs.

.yellow.b[Type de projet]  : Projet en maintenance

.yellow.b[Statut de projet]  : MCO

.yellow.b[Référent client]  : Séverine GERGAUD, Laurence DUMAY

]

.pull-right[

### Objectif septembre 2021

- Mise à jour des sources (fichiers fonciers, cadastre, DVF…)
- Extraction et diffusion trimestrielle des données. 

.yellow.b[coût estimé] : faible (qq jours)

.yellow.b[Pilote du projet]  : Philippe Terme

]
---

## SICLOP

.pull-left[
### Objectifs du projet
  
- Améliorer la connaissance du parc locatif privé
- Alimenter les politiques de l’habitat

.yellow.b[Type de projet]  : Projet en maintenance

.yellow.b[Statut de projet]  : MCO

.yellow.b[Référent client]  : Julien CAUDRELIER, Elsa Bertin

]

.pull-right[

### Objectif septembre 2021

Mettre à jour mensuellement le site de datavisualisation.

.yellow.b[coût estimé] : faible (qq jours)

.yellow.b[Pilote du projet]  : Maël Theulière

]
---
## Étude sur la tension dans le parc locatif social (TPLS) – Édition 2021

.pull-left[
### Objectifs du projet
  
Préparation des données du Créha Ouest. L'analyse et la valorisation sera effectuée par un stagiaire au SIAL.

.yellow.b[Type de projet]  : Projet nouveau

.yellow.b[Statut de projet]  : Actif

.yellow.b[Référent client]  : Pierre PIGNON, Valérie HUGAIN

]

.pull-right[

### Objectif septembre 2021

Traitement de la donnée nécessaire au stagiaire du SIAL
 
.yellow.b[coût estimé] : faible à modéré

.yellow.b[Pilote du projet]  : Denis Douillard

]
---
class: inverse, center, middle

# SRNP
---
## Déclinaison territoriale de la stratégies "Aires protégées"

.pull-left[
### Objectifs du projet
  
Production de cartes à l’échelle départementale ou infra pour prioriser des actions de création, d'optimisation ou d'extension d’aires protégées.

.yellow.b[Type de projet]  : Projet nouveau

.yellow.b[Statut de projet]  : Actif

.yellow.b[Référent client]  : Thomas OBÉ

]

.pull-right[

### Objectif septembre 2021

- Créer des cartes par département ou infra pour prioriser des actions de création, optimisation, extension d’aires protégées
- Assembler les couches sous SIG
- Les mettre en forme éventuellement

.yellow.b[coût estimé] : faible à modéré

.yellow.b[Pilote du projet]  : Anne-Cécile SIMON


]

---
## SINP (Système d’information de l’inventaire du patrimoine naturel)

.pull-left[
### Objectifs du projet
  
Finaliser le déploiement du SINP et optimiser son fonctionnement en vitesse de croisière.

.yellow.b[Type de projet]  : Projet nouveau

.yellow.b[Statut de projet]  : Actif

.yellow.b[Référent client]  : Arnaud Le Nevé    

]

.pull-right[

### Objectif septembre 2021

- Intégration des données contrôlés par la DB (travail d’Océane Guillemet) dans la base de données de la DREAL

- Intégrer la sensibilité régionale des espèces dans les processus de visualisation des données de la base DREAL

.yellow.b[coût estimé] : modéré (qq semaines)

.yellow.b[Pilote du projet]  : Ronan Vignard

]
---
## Outil régional PARCE
.pull-left[
### Objectifs du projet
  
Disposer d’une base de données régionales et des représentation cartographiques des obstacles à la continuité écologiques. Outil partagé entre DDT(M) et DREAL. Articulation avec l’outil Osmose.

.yellow.b[Type de projet]  : Projet en cours

.yellow.b[Statut de projet]  : Actif

.yellow.b[Référent client]  : Aurélie TISSERAND

]

.pull-right[

### Objectif septembre 2021

- Reprise des données des DDT(M)
- Création d'un projet sous QGIS pour la saisie par la DDT(M) et visualisation : livraison d'un v1

.yellow.b[coût estimé] : important (plusieurs semaines)

.yellow.b[Pilote du projet]  : Christine GALLAIS-JOUADET, Ronan VIGNARD

]
---
## Outil régional Plans d’eau

.pull-left[
### Objectifs du projet
  
Finaliser la v1 de l’outil Plan d’eau : Disposer d’un outil commun DDT(M)/DREAL pour connaître et suivre les plans d’eau en région.

.yellow.b[Type de projet]  : Projet en cours

.yellow.b[Statut de projet]  : Actif

.yellow.b[Référent client]  : François-Jacques CHENAIS

]

.pull-right[

### Objectif septembre 2021

Livraison d’une v1.
  
.yellow.b[coût estimé] : important (plusieurs semaines)

.yellow.b[Pilote du projet]  : Ronan VIGNARD

]
---
## Captages

.pull-left[
### Objectifs du projet
  
- Mieux intégrer les info sur les captages dans le SGBD
- Disposer d’un site de dataviz en lien avec l’ARS (lien stratégie régionale captage)

.yellow.b[Type de projet]  : Projet en cours

.yellow.b[Statut de projet]  : Actif

.yellow.b[Référent client]  : Rodolphe JARRY

]

.pull-right[

### Objectif septembre 2021

Livraison d'une v2 intégrant les retours des DDT(M).

.yellow.b[coût estimé] : modéré (qq semaines)

.yellow.b[Pilote du projet]  : Anne-Cécile SIMON    

]
---
## Nitrates

.pull-left[
### Objectifs du projet
  
- poursuite de la maintenance opérationnelle du site de dataviz ; 

.yellow.b[Type de projet]  : Projet en maintenance

.yellow.b[Statut de projet]  : En stand-by

.yellow.b[Référent client]  : Etienne SIMON

]

.pull-right[

### Objectif septembre 2021

- Evol fonctionnelles du site de dataviz : 
- indicateur « suivi des captages AEP », 
- passage en P90 par année hydrologique, 
- intégration des indicateurs « flux »

.yellow.b[coût estimé] : important (plusieurs semaines)

.yellow.b[Pilote du projet]  : Maël Theulière

]
---
## Pesticides

.pull-left[
### Objectifs du projet
  
- poursuite de la maintenance opérationnelle du site de dataviz ; 
- intégration des données souterraines : étape importante car à ce stade aucune analyse ne peut être faite sur les eaux souterraines
- appui pour la publication d'une étude complète sur l'analyse des données phyto 

.yellow.b[Type de projet]  : Projet en maintenance

.yellow.b[Statut de projet]  : Maintenance évolutive

.yellow.b[Référent client]  : Etienne SIMON

]

.pull-right[

### Objectif septembre 2021

- Chargement des données 2019 
- intégration des données "eaux souterraines" dans le patrimoine de données et sur le site de dataviz.

.yellow.b[coût estimé] : faible à modéré

.yellow.b[Pilote du projet]  : Maël Theulière

]
---
## Création et publication de la servitude AC2 de la DREAL sur le GPU

.pull-left[
### Objectifs du projet
  
Produire la SUP AC2 au format CNIG et l’intégrer au GPU.

.yellow.b[Type de projet]  : Projet en maintenance

.yellow.b[Statut de projet]  : Maintenance évolutive

.yellow.b[Référent client]  : Marine ISABAL

]

.pull-right[

### Objectif septembre 2021

Publication effectuée en mars 2021

.yellow.b[coût estimé] : faible à modéré

.yellow.b[Pilote du projet]  : Philippe TERME

]

.yellow.b[Remarque] : Nouvelle publication éventuelle en 2021 si classements de sites.
---
class: inverse, center, middle
# SRNT
---
## Création et publication de la servitude PM2 de la DREAL sur le GPU
.pull-left[
### Objectifs du projet
  
Produire la SUP PM2 au format CNIG et l’intégrer au GPU.

.yellow.b[Type de projet]  : Projet en cours

.yellow.b[Statut de projet]  : Actif

.yellow.b[Référent client]  : Héléne MORIN, Nicolas BOUDESSEUL

]

.pull-right[

### Objectif septembre 2021

Publication de la SUP.

.yellow.b[coût estimé] : faible à modéré

.yellow.b[Pilote du projet]  : Philippe TERME

]
---
## Migration S3IC - GUN
.pull-left[
### Objectifs du projet
  
L’outil GUN remplacera à partir de juin 2021 l’outil S3IC. Plusieurs chaînes d’intégration seront impactées. SRNT tient à préserver l’automatisation de l’extraction de ces différentes informations, enfin, d’une part d’informer le grand public et les acteurs publics via SIGLOIRE et, d’autre part de poursuivre l’alimentation des systèmes de valorisation mis en place par SCTE comme l’outil pour les carrières. SRNT demande à SCTE d’en étudier la faisabilité au cours du premier trimestre 2021 et de réaliser les nouveaux développements dans les meilleurs délais.

.yellow.b[Type de projet]  : Projet nouveau

.yellow.b[Statut de projet]  : Actif

.yellow.b[Référent client]  : Marie BARDON

]

.pull-right[

### Objectif septembre 2021

A définir (pilote de projet actuellement en arrêt)

.yellow.b[coût estimé] : important (plusieurs semaines)

.yellow.b[Pilote du projet]  : Philippe TERME

]
---
## Base de données sur l’eau pour ICPE les Pays de la Loire
.pull-left[
### Objectifs du projet
  
Contribuer à prévenir et gérer les impacts des ICPE, au regard des pressions exercées et de l’état des masses d’eau de la région.

.yellow.b[Type de projet]  : Projet en cours

.yellow.b[Statut de projet]  : En stand-by

.yellow.b[Référent client]  : Sophie LAVIGNE

]

.pull-right[

### Objectif septembre 2021

Première version de l'application de consultation.

.yellow.b[coût estimé] : important (plusieurs semaines)

.yellow.b[Pilote du projet]  : Daniel Kalioudjoglou

]
---
class: inverse, center, middle
# STRV
---
## Automatisation publication « La fréquentation des TCU »
.pull-left[
### Objectifs du projet
  
Automatiser la publication « La fréquentation des TCU ».

.yellow.b[Type de projet]  : Projet en maintenance

.yellow.b[Statut de projet]  : En stand-by

.yellow.b[Référent client]  : Didier Vivant

]

.pull-right[

### Objectif septembre 2021

Mise à jour de la publication avec les données de 2020.

.yellow.b[coût estimé] : modéré (qq semaines)

.yellow.b[Pilote du projet]  : Denis Douillard

]
---
## Automatisation publication « L’emploi dans les transports »
.pull-left[
### Objectifs du projet
  
Automatiser la publication « L’emploi dans les transports ».

.yellow.b[Type de projet]  : Projet en maintenance

.yellow.b[Statut de projet]  : En stand-by

.yellow.b[Référent client]  : Didier Vivant

]

.pull-right[

### Objectif septembre 2021

Mise à jour de la publication avec les données de 2020.

.yellow.b[coût estimé] : modéré (qq semaines)

.yellow.b[Pilote du projet]  : Denis Douillard

]
---
class: middle, center, title-slide

# Merci
